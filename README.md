# GoLang HTTP Auth Server

###Your mission should you choose to accept it is:

- Create a GoLang server that operates on port 8889
- Accepts POST type requests from a single path `/auth` expecting a username & password.
- Compare this username and password to a database ( of your choice ) of credentials and return a randomly generated token as a response if it passes.

> GoLang

> -> HTTP:8889

> --> /auth

> ----> DB Lookup


> -------> 200 Ok

> -----------> Generate Token

> <---------------------------------|


> --------> 401 Unauthorized

> <--------------------------------|